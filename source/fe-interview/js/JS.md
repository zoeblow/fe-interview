---
title: JS 基础
type: guide
order: 610
---

### JS 的数据类型有哪些

基本数据类型：共有7种

> Boolean Number String undefined null Bigint Symbol

Symbol ： ES6 引入的一种新的原始值，表示独一无二的值，主要为了解决属性名冲突问题。 Bigint ：ES2020 新增加，是比 Number 类型的整数范围更大。

引用数据类型：1种

> Object对象(包括普通Object、Function、Array、Date、RegExp、Math)

### JS 中有那 6 个值可以转换成 false

> undefined、 Null、 0(零)、 -0(负零)、 ''(空字符串)、 NaA

### JS 中 Null 与 Undefined 的区别

- 相同点
  都是一个值、都不能调用方法、 都表示无转换成布尔值都是 false
- 不同点
  1、Null 是关键字，undefined 不是
  2、Null 是一个空的 object undefined 是 Window 的一个属性
  3、typeof Null 返回 Object undefined 返回 undefined
  4、Null 可以装换为数值型的 0，undefined 职能转换为 NaN
- 其他
  Undefined 类型只有一个值，即 undefined。当声明的变量还未被初始化时，变量的默认值为 undefined。
  Null 类型也只有一个值，即 Null。Null 用来表示尚未存在的对象，常用来表示函数企图返回一个不存在的对象。

### JS 中 0.1+0.2 为什么不等于 0.3？

计算机中所有的计算都是转换成二进制进行计算的，0.1 和 0.2 在计算机有效的位数之内都不能转换成精确地二进制的值，所以计算机只能取个近似值。

### split() join() 的区别

前者是切割成数组的形式，后者是将数组转换成字符串

### 数组方法 pop() push() unshift() shift()

- Push()尾部添加
- pop()尾部删除
- Unshift()头部添加
- shift()头部删除

### JSON 的了解？

JSON(JavaScript Object Notation) 是一种轻量级的数据交换格式。

### ajax 是什么

AJAX 的全称是 Asynchronous JavaScript and XML（异步的 JavaScript 和 XML）。

- ajax 的优点：
  1、最大的一点是页面无刷新，用户的体验非常好。
  2、使用异步方式与服务器通信，具有更加迅速的响应能力。。
  3、可以把以前一些服务器负担的工作转嫁到客户端，利用客户端闲置的能力来处理，减轻服务器和带宽的负担，节约空间和宽带租用成本。并且减轻服务器的负担，ajax 的原则是“按需取数据”，可以最大程度的减少冗余请求，和响应对服务器造成的负担。
  4、基于标准化的并被广泛支持的技术，不需要下载插件或者小程序。
  5、ajax 可使因特网应用程序更小、更快，更友好。

- ajax 的缺点：
  1、ajax 不支持浏览器 back 按钮。
  2、安全问题 AJAX 暴露了与服务器交互的细节。
  3、对搜索引擎的支持比较弱。
  4、破坏了程序的异常机制。
  5、不容易调试。
- Get 请求和 Post 请求的区别
  1、使用 Get 请求时,参数在 URL 中显示,而使用 Post 方式,则不会显示出来
  2、使用 Get 请求发送数据量小,Post 请求发送数据量大

### 定义变量的时候使用 var 和不适用 var 有什么不同？

- 1、使用 var 是局部变量，不使用是全部变量，使用 var 有一定的作用域，不使用 var 是在 window 全局顶一个属性。
- 2、不使用 var 定义可以被删除，因为她是一个 Windows 的属性
- 3、var 定义的变量可以被提前，不使用 var 不能提前

### JavaScript 是如何实现继承

- 1.原型链 基本思想：利用原型让一个引用类型继承另外一个引用类型的属性和方法
- 2.借用构造函数 基本思想：在子类型构造函数的内部调用超类构造函数，通过使用 call()和 apply()方法可以在新创建的对象上执行构造函数。
