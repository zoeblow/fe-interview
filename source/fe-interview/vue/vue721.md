---
title: vue路由的钩子函数
type: guide
order: 721
---

首页可以控制导航跳转，beforeEach，afterEach 等，一般用于页面 title 的修改。一些需要登录才能调整页面的重定向功能。

beforeEach 主要有 3 个参数 to，from，next：

to：route 即将进入的目标路由对象，

from：route 当前导航正要离开的路由

next：function 一定要调用该方法 resolve 这个钩子。执行效果依赖 next 方法的调用参数。可以控制网页的跳转。
