---
title: HTML 基础面试题
type: guide
order: 410
---

### 行内元素有哪些？块级元素有哪些？ 空(void)元素有那些？

> 1、CSS 规范规定，每个元素都有 display 属性，确定该元素的类型，每个元素都有默认的 display 值，比如 div 默认 display 属性值为“block”，成为“块级”元素；span 默认 display 属性值为“inline”，是“行内”元素。
> 2、行内元素有：a b span img input select strong（强调的语气） 块级元素有：div ul ol li dl dt dd h1 h2 h3 h4…p
> 3、知名的空元素：

```html
<br />
<hr />
<img /><input /><link /><meta />
```

行内和块级元素的区别？

> 行内元素支持宽高属性，或者上下的边距（margin）吗？怎么让他们支持，有几种办法？

### 常见的伪类 和 为元素有哪些？

> 伪类 :active :hover :focus :link :visited :first-child :lang
> 为元素 :before :after :first-letter :first-line
> 怎么区分他们
> 1、一个冒号或者两个冒号
> 2、能不能再 dom 上找到

### 盒模型

页面渲染时，dom 元素所采用的 布局模型。可通过box-sizing进行设置。根据计算宽高的区域可分为：

> content-box (W3C 标准盒模型)
> border-box (IE 盒模型)
> padding-box (FireFox 曾经支持)
> margin-box (浏览器未实现)

理论上是有上面 4 种盒子，但现在 w3c 与 mdn 规范中均只支持 content-box 与 border-box

### 标准 W3C 盒子模型和 IE 盒子模型有什么区别

> 标准 w3c 盒子模型的范围包括 margin、border、padding、content，并且 content 部> 不包含其他部分。
> ie 盒子模型的范围也包括 margin、border、padding、content，
> ie 盒子模型的 content 部分包含了 border 和 pading。

### Chrome 中文界面下默认会将小于 12px 的文本强制按照 12px 显示怎么解决

> 使用 CSS3 的缩放功能 transform: scale(.8);

### 你知道那些 HTML5 的新特性 ？那些 HTML5 的新的特性在你的工作中使用过？

> canvas localStorage sessionStorage
> HTML5 的离线储存？
> localStorage sessionStorage 的区别
> localStorage 长期存储数据，浏览器关闭后数据不丢失；
> sessionStorage 数据在浏览器关闭后自动删除。

### 如何实现浏览器内多个标签页之间的通信?

> 调用 localstorge、cookies 等本地存储方式

### iframe 有那些缺点？

> 1、iframe 会阻塞主页面的 Onload 事件；
> 2、iframe 和主页面共享连接池，而浏览器对相同域的连接有限制，所以会影响页面的并行载。

 有什么办法优化他们吗？
> 使用 iframe 之前需要考虑这两个缺点。如果需要使用 iframe，最好是通过 javascript 动态 iframe 添加 src 属性值，这样可以可以绕开以上两个问题
