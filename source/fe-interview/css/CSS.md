---
title: CSS 基础面试题
type: guide
order: 510
---

### CSS 选择器优先级

> * !important > 行内样式 > #id > .class > tag > * > 继承 > 默认
> * 选择器 从右往左 解析

### 一个定宽定高的 div 如何在浏览器窗口垂直水平居中？

> 绝对定位，利用负 margin
> 利用 CSS3 的 transform

### BFC

IE下为 Layout，可通过 zoom:1 触发

**块级格式化上下文**，是一个独立的渲染区域，让处于 BFC 内部的元素与外部的元素相互隔离，使内外元素的定位不会相互影响。

触发条件：

> * 根元素
> * position: absolute/fixed
> * display: inline-block / table
> * float 元素
> * ovevflow !== visible

规则：

> * 属于同一个 BFC 的两个相邻 Box 垂直排列
> * 属于同一个 BFC 的两个相邻 Box 的 margin 会发生重叠
> * BFC 中子元素的 margin box 的左边， 与包含块 (BFC) border box的左边相接触 (子元素 absolute 除外)
> * BFC 的区域不会与 float 的元素区域重叠
> * 计算 BFC 的高度时，浮动子元素也参与计算
> * 文字层不会被浮动层覆盖，环绕于周围

应用:

> * 阻止margin重叠
> * 可以包含浮动元素 —— 清除内部浮动(清除浮动的原理是两个div都位于同一个 BFC 区域之中)
> * 自适应两栏布局
> * 可以阻止元素被浮动元素覆盖

### CSS 清除浮动的几种方式?

> 父级 div 定义 height
> 结尾处加空 div 标签 clear:both
> 父级 div 定义 伪类:after 和 zoom
> 父级 div 定义 overflow:hidden /auto
> 父级 div 也一起浮动
> 父级 div 定义 display:table

### 居中布局

水平居中：

> * 行内元素: text-align: center
> * 块级元素: margin: 0 auto
> * absolute + transform
> * flex + justify-content: center

垂直居中：

> * line-height: height
> * absolute + transform
> * flex + align-items: center
> * table

### CSS预处理器的原理: 是将类 CSS 语言通过 Webpack 编译 转成浏览器可读的真正 CSS。在这层编译之上，便可以赋予 CSS 更多更强大的功能，常用功能:

> 嵌套
> 变量
> 循环语句
> 条件语句
> 自动前缀
> 单位转换
> mixin复用

### 如果需要手动写动画，你认为最小时间间隔是多久，为什么?

> 多数显示器默认频率是 60Hz，即 1 秒刷新 60 次，所以理论上最小间隔为 1/60＊1000ms ＝ 16.7ms

### display:inline-block 什么时候会显示间隙？

> 移除空格、使用 margin 负值、使用 font-size:0、letter-spacing、word-spacing

### link 与 @import 的区别

> link功能较多，可以定义 RSS，定义 Rel 等作用，而@import只能用于加载 css
> 当解析到link时，页面会同步加载所引的 css，而@import所引用的 css 会等到页面加载完才被加载
> @import需要 IE5 以上才能使用
> link可以使用 js 动态引入，@import不行
