---
title: Fe-interview介绍
type: guide
order: 2
---

这是一份自己总结的关于准备前端面试的一个复习汇总项目

STAR 面试法

STAR 面试法是企业招聘面试过程中可采用的技巧。其中，“STAR”是 SITUATION（背景）、TASK（任务）、ACTION（行动）和 RESULT（结果）四个英文单词的首字母组合。

请讲出一件你通过学习尽快胜任新的工作任务的事。追问：
（1）这件事发生在什么时候？---------------------S
（2） 你要从事的工作任务是什么？--------------T
（3） 接到任务后你怎么办？-----------------------A
（4） 你用了多长时间获得完成该任务所必须的知识？------深层次了解
（5） 你在这个过程中遇见困难了吗？ -------------------顺便了解坚韧性
（6） 你最后完成任务的情况如何？--------------R
