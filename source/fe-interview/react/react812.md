---
title: setState 背后的批量更新如何实现
type: guide
order: 812
---

想必大家都知道大部分情况下多次 setState 不会触发多次渲染，并且 state 的值也不是实时的，这样的做法能够减少不必要的性能消耗。

```js
handleClick () {
  // 初始化 `count` 为 0
  console.log(this.state.count) // -> 0
  this.setState({ count: this.state.count + 1 })
  this.setState({ count: this.state.count + 1 })
  console.log(this.state.count) // -> 0
  this.setState({ count: this.state.count + 1 })
  console.log(this.state.count) // -> 0
}
```

那么这个行为是如何实现的呢？答案是批量更新。接下来我们就来学习批量更新是如何实现的。

其实这个背后的原理相当之简单。假如 `handleClick` 是通过点击事件触发的，那么 `handleClick` 其实差不多会被包装成这样：

```js
isBatchingUpdates = true;
try {
  handleClick();
} finally {
  isBatchingUpdates = false;
  // 然后去更新
}
```

在执行 `handleClick` 之前，其实 `React` 就会默认这次触发事件的过程中如果有 `setState` 的话就应该批量更新。

当我们在 `handleClick` 内部执行 `setState` 时，更新状态的这部分代码首先会被丢进一个队列中等待后续的使用。然后继续处理更新的逻辑，毕竟触发 `setState` 肯定会触发一系列组件更新的流程。但是在这个流程中如果 `React` 发现需要批量更新 state 的话，就会立即中断更新流程。

也就是说，虽然我们在 `handleClick` 中调用了三次 `setState`，但是并不会走完三次的组件更新流程，只是把更新状态的逻辑丢到了一个队列中。当 `handleClick` 执行完毕之后会再执行一次组件更新的流程。

另外组件更新流程其实是有两个截然不同的分支的。一种就是触发更新以后一次完成全部的组件更新流程；另一种是触发更新以后分时间片段完成所有的组件更新，用户体验更好，这种方式被称之为任务调度。如果你想详细了解这一块的内容，可以阅读我之前 写的文章。

当然本文也会提及一部分调度相关的内容，毕竟这块也包含在组件更新流程中。但是在学习任务调度之前，我们需要先来学习下 fiber 相关的内容，因为这块内容是 `React` 实现各种这些新功能的基石。
