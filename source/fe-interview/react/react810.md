<!--
 * @Description: 
 * @Author: zoeblow
 * @Email: wangfuyuan@nnuo.com
 * @Date: 2020-06-28 15:54:20
 * @LastEditors: zoeblow
 * @LastEditTime: 2020-09-18 16:21:56
 * @FilePath: /fe-interview/source/fe-interview/react/react810.md
-->
---
title: React
type: guide
order: 810
---

## react 技术点(针对有开发项目的)

### class 组件

1.  react 生命周期有哪些，按运行阶段生命周期调用顺序
    - react 生命周期分为初始化阶段、运行阶段、销毁阶段。 - 初始化阶段：
      componentWillMount：实例挂载之前 - Render：渲染组件
      componentDidMount：实例挂载完成。一般在这个函数中与后台进行初始化数据交互 - 运行阶段： - componentWillReceiveProps：父组件改变时调用。注意只要父组件此方法触发，那么子组件也会触发，也就是说父组件更新，子组件也会跟着更新。 - state 发生改变时更新 - sholudComponentUpdate：主要是用来手动阻止组件渲染，一般在这个函数中做组件的性能优化。方法在接收了新的 props 或 state 后触发，你可以通过返回 true 或 false 来控制后面的生命周期流程是否触发。方法在 setState 后 state 发生改变后触发，你可以通过返回 true 或 false 来控制后面的生命周期流程是否触发。 - componentWillUpdate：组件数据更新前调用 在 state 改变或 shouldComponentUpdate 返回 true 后触发。不可在其中使用 setState - render - componentDidUpdate：组件数据更新完成时调用 - 销毁阶段： - componentWillUnmount：销毁阶段。一般用来销毁不用的变量或者是解除无用定时器以及解绑无用事件。防止内存泄漏问题。
2.  react key 的作用
    - key 是 React 中用于追踪哪些列表中元素被修改、删除或者被添加的辅助标识。在 diff 算法中，key 用来判断该元素节点是被移动过来的还是新创建的元素，减少不必要的元素重复渲染。
    - （使用下标作为 KEY 常见问题）
      - 🌰：一组依赖 state 渲染的输入框使用下标作为 key,对数据进行增删改查，输入框渲染就出问题了
3.  setState 第二个参数的作用
    - setState 是一个异步的过程，所以说执行完 setState 之后不能立刻更改 state 里面的值。如果需要对 state 数据更改监听，setState 提供第二个参数，就是用来监听 state 里面数据的更改，当数据更改完成，调用回调函数。
4.  高阶组件
    - 高阶组件就是一个函数，且该函数接受一个组件作为参数，并返回一个新的组件。
    - 什么时候会用到？
      - 🌰：当组件拥有相同的绑定事件，view 展示不同时可以考虑高阶组件实现
5.  react 中组件传值
    - 父传子（组件嵌套浅）：父组件定义一个属性，子组件通过 this.props 接收。
    - 子传父：父组件定义一个属性，并将一个回调函数赋值给定义的属性，然后子组件进行调用传过来的函数，并将参数传进去，在父组件的回调函数中即可获得子组件传过来的值。
    - 深层嵌套组件传值怎么处理？
      - 引出数据状态管理器（redux）

### 进阶

1. 生命周期改变
   - static getDerivedStateFromProps：这个生命周期函数是为了替代 componentWillReceiveProps 存在的，所以在你需要使用 componentWillReceiveProps 的时候，就可以考虑使用 getDerivedStateFromProps 来进行替代了。这个生命周期必须有返回
     异步数据更新：需要搭配 componentDidUpdate，服务端请求可以放在该生命周期
2. react 组件异常捕获
   - static getDerivedStateFromError 和 componentDidCatch 区别
3. react 怎么实现组件懒加载
   - lazy 和 Suspense
   - 特别要注意会不会考虑无法加载组件异常，常用 componentDidCatch
4. 性能优化（这个可以夹带在生命周期里面提问）
   - 重写 shouldComponentUpdate 来避免不必要的 dom 操作。
   - 使用 key 来帮助 React 识别列表中所有子组件的最小变化。
5. forceUpdate 作用
   - forceUpdate() 就是强制运行 render，有些变量不在 state 上，但是又想达到 view 更新，重新 render 的效果的时候，就可以使用此方法手动触发 render
