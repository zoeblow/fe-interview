<!--
 * @Description: README
 * @Author: zoeblow
 * @Email: wangfuyuan@nnuo.com
 * @Date: 2020-05-26 14:06:05
 * @LastEditors: zoeblow
 * @LastEditTime: 2023-06-30 13:32:24
 * @FilePath: \fe-interview\README.md
-->

# fe-interview

## 介绍

前端面试

## 开发

```bash
# 详情看package.json

# 启动
yarn server

# 编译
yarn build
```

## 分类

- HTML
- CSS
- JS
- Vue
- React
- 其他

## 书写规范

### 文件名 按照类型 + order 编号进行命名

示例

```js
vue711.md;
```

### 头部

需要标明 标题 和 order

标题是显示在左侧列表和顶部的文字 order 是显示的位置顺序

示例

```js
---
title: 对于MVVM的理解？
type: guide
order: 711
---
```

### order 取值范围

| 分类  |     条目     |  范围   |
| :---: | :----------: | :-----: |
| HTML  | HTML 面试题  | 410-499 |
|  CSS  |  CSS 面试题  | 510-599 |
|  JS   |  JS 面试题   | 610-699 |
|  Vue  |  Vue 面试题  | 710-799 |
| React | React 面试题 | 810-899 |
| 其他  | 其他 面试题  | 910-999 |
|   -   |      -       |    -    |

### 图片

全部采用本地图片
存放位置为 `/images/类型/order+编号.格式`

示例

此图表示 在 vue 目录下的 711 编号的第 1 张图

```js
![mvvm](/images/vue/711-1.png)
```
